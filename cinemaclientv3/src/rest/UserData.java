package rest;

import java.io.Serializable;
import java.util.ArrayList;

public class UserData implements Serializable {
     String firstName;
     String lastName;
     String email;
     Integer film_id;
     ArrayList<Integer> listOfReservedSeats;
     Integer price;

    public UserData(String firstName, String lastName, String email, Integer film_id, ArrayList<Integer> listOfReservedSeats) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.film_id = film_id;
        this.listOfReservedSeats = listOfReservedSeats;
        this.price = new Price(16,30,listOfReservedSeats).summaryOfPrice();
    }

    @Override
    public String toString() {
        return "UserData{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", film_id=" + film_id +
                ", listOfReservedSeats=" + listOfReservedSeats +
                ", price=" + price +
                '}';
    }
}
