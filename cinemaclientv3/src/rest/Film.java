package rest;



import java.util.ArrayList;


public class Film {
    int film_id;
    String film_title;
    String film_date;
    String film_hour;
    ArrayList<Seats> reservedOrNot_array;


    public Film(int film_id, String film_title, String film_date, String film_hour, ArrayList<Seats> reservedOrNot_array) {
        this.film_id = film_id;
        this.film_title = film_title;
        this.film_date = film_date;
        this.film_hour = film_hour;
        this.reservedOrNot_array=reservedOrNot_array;
    }

    public int getFilm_id() {
        return film_id;
    }

    public void setFilm_id(int film_id) {
        this.film_id = film_id;
    }

    public String getFilm_title() {
        return film_title;
    }

    public void setFilm_title(String film_title) {
        this.film_title = film_title;
    }

    public String getFilm_date() {
        return film_date;
    }

    public void setFilm_date(String film_date) {
        this.film_date = film_date;
    }

    public String getFilm_hour() {
        return film_hour;
    }

    public void setFilm_hour(String film_hour) {
        this.film_hour = film_hour;
    }

    public ArrayList<Seats> getReservedOrNot_array() {
        return reservedOrNot_array;
    }

    public void setReservedOrNot_array(ArrayList<Seats> reservedOrNot_array) {
        this.reservedOrNot_array = reservedOrNot_array;
    }

    @Override
    public String toString() {
        return "Film{" +
                "film_id=" + film_id +
                ", film_title='" + film_title + '\'' +
                ", film_date='" + film_date + '\'' +
                ", film_hour='" + film_hour + '\'' +
                ", reservedOrNot_array=" + reservedOrNot_array +
                '}';
    }
}
