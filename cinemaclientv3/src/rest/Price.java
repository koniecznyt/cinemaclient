package rest;

import java.util.ArrayList;

public class Price {
    int normalPrice;
    int vipPrice;
    ArrayList<Integer> listOfReservedSeats;

    public Price(int normalPrice, int vipPrice, ArrayList<Integer> listOfReservedSeats) {
        this.normalPrice = normalPrice;
        this.vipPrice = vipPrice;
        this.listOfReservedSeats = listOfReservedSeats;
    }

    public Integer summaryOfPrice(){
        Integer summary=0;
        for(int i=0;i<this.listOfReservedSeats.size();i++){
            if(this.listOfReservedSeats.get(i)>24){
                summary=summary+vipPrice;
            }
            else{
                summary=summary+normalPrice;
            }
        }
        return summary;
    }
}
