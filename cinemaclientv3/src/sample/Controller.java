package sample;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Ellipse;
import rest.Price;
import rest.Seats;
import rest.UserData;

import java.util.ArrayList;


public class Controller {

    //Poniżej - Pobieranie obiektów GUI z sample.fxml

@FXML
    CheckBox ticket_checkbox,checkbox_1, checkbox_2, checkbox_3, checkbox_4,checkbox_5,checkbox_6,checkbox_7,checkbox_8,checkbox_9,checkbox_10,checkbox_11,checkbox_12,checkbox_13,checkbox_14,checkbox_15,checkbox_16,checkbox_17,checkbox_18,checkbox_19,checkbox_20,checkbox_21,checkbox_22,checkbox_23,checkbox_24,checkbox_25,checkbox_26,checkbox_27,checkbox_28;
@FXML
    Button accept_button;
@FXML
    TextField client_name, client_surname, client_email, amount_textlabel,film_name_textfield,free_seats,reserved_seats;
@FXML
    ComboBox reservation_day, reservation_hour;
@FXML
    Circle norm1,norm2,norm3,norm4,norm5,norm6,norm7,norm8,norm9,norm10,norm11,norm12,norm13,norm14,norm15,norm16,norm17,norm18,norm19,norm20,norm21,norm22,norm23,norm24;
@FXML
    Ellipse vip01,vip02,vip03,vip04;


    public static Integer filmId=0;


//Poniżej - Metody obsługi GUI

    public ArrayList<Integer> checkWhatSeatIsReservedByClient(){
        ArrayList<Integer> ArrayReservedSeats = new ArrayList<>();
        CheckBox[] checkBoxes = new CheckBox[28];
        checkBoxes[0]=checkbox_1;checkBoxes[1]=checkbox_2;checkBoxes[2]=checkbox_3;checkBoxes[3]=checkbox_4;checkBoxes[4]=checkbox_5;checkBoxes[5]=checkbox_6;checkBoxes[6]=checkbox_7;checkBoxes[7]=checkbox_8;checkBoxes[8]=checkbox_9;checkBoxes[9]=checkbox_10;checkBoxes[10]=checkbox_11;checkBoxes[11]=checkbox_12;checkBoxes[12]=checkbox_13;checkBoxes[13]=checkbox_14;checkBoxes[14]=checkbox_15;checkBoxes[15]=checkbox_16;checkBoxes[16]=checkbox_17;checkBoxes[17]=checkbox_18;checkBoxes[18]=checkbox_19;checkBoxes[19]=checkbox_20;checkBoxes[20]=checkbox_21;checkBoxes[21]=checkbox_22;checkBoxes[22]=checkbox_23;checkBoxes[23]=checkbox_24;checkBoxes[24]=checkbox_25;checkBoxes[25]=checkbox_26;checkBoxes[26]=checkbox_27;checkBoxes[27]=checkbox_28;
        for(int i=0;i<checkBoxes.length;i++){
            if(checkBoxes[i].isSelected()==true){
                ArrayReservedSeats.add(i+1);  //WARTOSCI W LISCIE ZWRACANE +1 (brak 0, wartość max=28)
            }
        }
    return ArrayReservedSeats;
    }

    public String getName(){return client_name.getText();}
    public String getSurname(){return client_surname.getText();}
    public String getEmail(){return client_email.getText();}

    public boolean makeSureSelectedSeatsArentReserved(){
        ArrayList <Seats> listFromServer = httpLinking.getFilmFromServer().getReservedOrNot_array();
        ArrayList <Integer> listSelectedSeatsFromClient = checkWhatSeatIsReservedByClient();
        ArrayList <Integer>  tempListReservedIDsFromServer = new ArrayList<>();
        for(int i=0;i<listFromServer.size();i++){
            if(listFromServer.get(i).getStatus()==1)tempListReservedIDsFromServer.add(listFromServer.get(i).getId());
        }
        for(int i = 0 ; i < listSelectedSeatsFromClient.size() ; i ++){
            for(int k=0;k<tempListReservedIDsFromServer.size();k++){
                if(listSelectedSeatsFromClient.get(i)==tempListReservedIDsFromServer.get(k)) return false;
            }
        }
        return true;
    }

    public void refreshAfterReservation(){
        setHowManyReservedSeats();
        setHowManyFreeSeats();
        imageOfCinemaColorIn_reserved();
        imageOfCinemaColorIn_free();;

    }

    public void setHowManyFreeSeats(){
        free_seats.setText(Integer.toString(ReservatonFuctions.freeSeatsArray().size()));
    }
    public void setHowManyReservedSeats(){
        reserved_seats.setText(Integer.toString(ReservatonFuctions.reservedSeatsArray().size()));
    }

    public void imageOfCinemaColorIn_reserved(){
        Ellipse[] tabEllipses = {vip01, vip02, vip03, vip04};
        Circle[] tabCircles = {norm1, norm2, norm3, norm4, norm5, norm6, norm7, norm8, norm9, norm10, norm11, norm12, norm13, norm14, norm15, norm16, norm17, norm18, norm19, norm20, norm21, norm22, norm23, norm24};
        ArrayList<Seats> seatsTabFromServer = httpLinking.getFilmFromServer().getReservedOrNot_array();
        ArrayList<Integer> tempTabReserved = new ArrayList<>(); //zapisuje ID (numery miejsc) ktore sa 1(zajete)
        for(int i=0;i<seatsTabFromServer.size();i++) {
            if (seatsTabFromServer.get(i).getStatus()==1){
                tempTabReserved.add(seatsTabFromServer.get(i).getId());
            }
        }
        for(int i=1;i<=tabCircles.length;i++){
            for(int j=0;j<tempTabReserved.size();j++){
                if(tempTabReserved.get(j)==(i)){
                    tabCircles[(i-1)].setFill(Color.rgb(255,0,0));
                }
            }
        }
        for(int j=0;j<tempTabReserved.size();j++) {
            if (tempTabReserved.get(j) == 25) tabEllipses[0].setFill(Color.rgb(255, 0, 0));
            if (tempTabReserved.get(j) == 26) tabEllipses[1].setFill(Color.rgb(255, 0, 0));
            if (tempTabReserved.get(j) == 27) tabEllipses[2].setFill(Color.rgb(255, 0, 0));
            if (tempTabReserved.get(j) == 28) tabEllipses[3].setFill(Color.rgb(255, 0, 0));
        }
    }

    public void imageOfCinemaColorIn_free() {
        Ellipse[] tabEllipses = {vip01, vip02, vip03, vip04};
        Circle[] tabCircles = {norm1, norm2, norm3, norm4, norm5, norm6, norm7, norm8, norm9, norm10, norm11, norm12, norm13, norm14, norm15, norm16, norm17, norm18, norm19, norm20, norm21, norm22, norm23, norm24};
        ArrayList<Seats> seatsTabFromServer = httpLinking.getFilmFromServer().getReservedOrNot_array();
        ArrayList<Integer> tempTabFree = new ArrayList<>(); //zapisuje ID (numery miejsc) ktore sa 0(wolne)
        for(int i=0;i<seatsTabFromServer.size();i++) {
            if (seatsTabFromServer.get(i).getStatus()==0){
                tempTabFree.add(seatsTabFromServer.get(i).getId());
            }
        }
        for(int i=1;i<=tabCircles.length;i++){
            for(int j=0;j<tempTabFree.size();j++){
                if(tempTabFree.get(j)==i){
                    tabCircles[(i-1)].setFill(Color.rgb(0,255,0));
                }
            }
        }

        for(int j=0;j<tempTabFree.size();j++) {
            if (tempTabFree.get(j) == 25) tabEllipses[0].setFill(Color.rgb(0, 255, 0));
            if (tempTabFree.get(j) == 26) tabEllipses[1].setFill(Color.rgb(0, 255, 0));
            if (tempTabFree.get(j) == 27) tabEllipses[2].setFill(Color.rgb(0, 255, 0));
            if (tempTabFree.get(j) == 28) tabEllipses[3].setFill(Color.rgb(0, 255, 0));
        }


    }



    //PONIŻEJ FUKCJE AKCJI (onMauseClicked, ButtonClicked itd)

    public void filmKevinMouseClicked() {
        Controller.filmId=1;
        film_name_textfield.setText(httpLinking.getFilmFromServer().getFilm_title());
        reservation_day.setValue(httpLinking.getFilmFromServer().getFilm_date());
        reservation_hour.setValue(httpLinking.getFilmFromServer().getFilm_hour());
        setHowManyFreeSeats();
        setHowManyReservedSeats();
        imageOfCinemaColorIn_reserved();
        imageOfCinemaColorIn_free();
    }
    public void filmStarWarsMouseClicked()
    {
        Controller.filmId=2;
        film_name_textfield.setText(httpLinking.getFilmFromServer().getFilm_title());
        reservation_day.setValue(httpLinking.getFilmFromServer().getFilm_date());
        reservation_hour.setValue(httpLinking.getFilmFromServer().getFilm_hour());
        setHowManyFreeSeats();
        setHowManyReservedSeats();
        imageOfCinemaColorIn_reserved();
        imageOfCinemaColorIn_free();
    }
    public void filmHpMouseClicked(){

        Controller.filmId=3;
        film_name_textfield.setText(httpLinking.getFilmFromServer().getFilm_title());
        reservation_day.setValue(httpLinking.getFilmFromServer().getFilm_date());
        reservation_hour.setValue(httpLinking.getFilmFromServer().getFilm_hour());
        setHowManyFreeSeats();
        setHowManyReservedSeats();
        imageOfCinemaColorIn_reserved();
        imageOfCinemaColorIn_free();
    }

    public void priceButtonClicked(){
        if(checkWhatSeatIsReservedByClient().isEmpty()){
            Alerts.seatsError();
        }
        else
        {
            amount_textlabel.setText(Integer.toString(new Price(16,30,checkWhatSeatIsReservedByClient()).summaryOfPrice()));
        }
    }

    public void acceptButtonClicked(){
        if(getName().isEmpty() || getSurname().isEmpty() || getEmail().isEmpty()){
            Alerts.noDataError();
        }
        else if(Controller.filmId==0){
            Alerts.noFilmError();
        }
        else if(checkWhatSeatIsReservedByClient().isEmpty()){
            Alerts.seatsError();
        }
        else if(makeSureSelectedSeatsArentReserved()==false){
            Alerts.sameSeatsAreReservedError();
        }
        else {
            UserData userData = new UserData(getName(), getSurname(), getEmail(), Controller.filmId, checkWhatSeatIsReservedByClient());
            httpLinking.PostDataToReservation(userData);
            refreshAfterReservation();
            priceButtonClicked();
            Alerts.allIsOKDialog();
            if(ticket_checkbox.isSelected()) httpLinking.getTicketPDF(userData);
        }

    }

}
