package sample;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import rest.Film;
import rest.UserData;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;

public class httpLinking {
    static void PostDataToReservation(UserData userData) {

        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/api/reservation/add");

        Gson gson = new Gson();

        // Tworzymy obiekt uzytkownika
        final UserData userDataFromClient = userData;

        // Serializacja obiektu do JSONa
        final String json = gson.toJson(userDataFromClient);

        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);

            System.out.println("Kod odpowiedzi serwera: " + response.getStatusLine().getStatusCode());

            if(response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Prosze poprawic w kontrolerze sciezke do pliku - sciezka jest nieprawidlowa!");
            } else if(response.getStatusLine().getStatusCode() == 201) {

                System.out.println("Rezerwacja wyslana!");
            }

            client.close();
        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }
    }


    public static Film getFilmFromServer() {

        final HttpClient client = HttpClientBuilder.create().build();

        /*
            Do konstruktora klasy HttpGet podajemy url z nasza usluga ktora zwaraca JSON'a.
            W tym miejscu tworzymy request serwera.
        */
        final HttpGet request = new HttpGet("http://127.0.0.1:8080/api/data/film/get/id/"+Controller.filmId);

        /* Przy pomocy tej biblioteki zmienimy JSON'a na obiekt typu 'Film'. */
        final Gson gson = new Gson();
        Film filmFromServerToReturn= null;
        try {

            final HttpResponse response = client.execute(request);  // Otrzymujemy odpowiedz od serwera.
            final HttpEntity entity = response.getEntity();

            final String json = EntityUtils.toString(entity);   // Na tym etapie odczytujemy JSON'a, ale jako String.


            /*
                Tutaj odbywa sie przetworzenie (serializacja) JSON'a (String) na rest.Film

             */

            final Type type = new TypeToken<Film>(){}.getType();
            final Film filmFromServer = gson.fromJson(json, type);


            if(response.getStatusLine().getStatusCode() == 400) {
                System.out.println("Brak danych do wyswietlenia!");
            } else if(response.getStatusLine().getStatusCode() == 200) {
              filmFromServerToReturn=filmFromServer;
              return  filmFromServer;
            }

        } catch (IOException e) {

            System.out.println("Houston, we have a problem with GET");
            e.printStackTrace();

        }
        return filmFromServerToReturn;
    }

    public static void getTicketPDF(UserData userData){
        final CloseableHttpClient client = HttpClients.createDefault();
        final HttpPost httpPost = new HttpPost("http://127.0.0.1:8080/api/reservation/print/ticket");
        Gson gson = new Gson();
        final UserData userDataFromClient = userData;
        final String json = gson.toJson(userDataFromClient);
        try {

            final StringEntity entity = new StringEntity(json);
            httpPost.setEntity(entity);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            final CloseableHttpResponse response = client.execute(httpPost);

            if(response.getStatusLine().getStatusCode() == 404) {

                System.out.println("Prosze poprawic w kontrolerze sciezke do pliku - sciezka jest nieprawidlowa!");
            } else if(response.getStatusLine().getStatusCode() == 201) {

                System.out.println("PDF zostal wygenerowany!");

            }

            client.close();
        } catch (UnsupportedEncodingException e) {

            System.out.println("Houston, we have a problem with POST unsupported encoding");
            e.printStackTrace();
        } catch (ClientProtocolException e) {

            System.out.println("Houston, we have a problem with POST client protocol");
            e.printStackTrace();
        } catch (IOException e) {

            System.out.println("Houston, we have a problem with POST input output");
            e.printStackTrace();
        }


    }
}
